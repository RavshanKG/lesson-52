import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

const Number = props => (
    <div>
        <span>{props.number}</span>
    </div>
);

class App extends Component {
    numbers = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36];

    state = {
        numbers: []
    };

    picknumber = function () {
        let numbersleft = [...this.numbers];
        console.log(numbersleft);
        let newNumbers = [];
        for (var i = 0; i < 5; i++){
            let randomNumbers = Math.floor(Math.random() * numbersleft.length);
            newNumbers.push(numbersleft[randomNumbers]);
            numbersleft.splice(randomNumbers, 1);
        }
        console.log(newNumbers);
        return newNumbers;
    };
    generateNumbers = () => {
        let newNumbers = this.picknumber();
        newNumbers.sort((a, b) => a < b);
        this.setState ({numbers: newNumbers});
    };

    componentDidMount = () => {
        this.generateNumbers();
        console.log(this.state);
    };
    start = () => {
        this.generateNumbers();
    };



  render() {
    return (
      <div className="App">
          <Number className="num" number={this.state.numbers[0]} />
          <Number className="num" number={this.state.numbers[1]} />
          <Number className="num" number={this.state.numbers[2]} />
          <Number className="num" number={this.state.numbers[3]} />
          <Number className="num" number={this.state.numbers[4]} />

{/*
          {this.state.numbers.map((number, index) => <Number key={index} number={number} />)}
*/}

          <div><button id="button" onClick={this.start}>Start!</button></div>
      </div>
    );
  }
}




export default App;
